<?php

namespace App\Cimangko;

use Yajra\Datatables\Datatables;
use Yajra\Datatables\Services\DataTable;

class CimangkoDatatables extends Datatables
{
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->parameters([
                        'buttons' => ['excel'],
                    ]);
    }

}