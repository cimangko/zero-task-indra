<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengembalian extends Model
{
  protected $appends = [
      'keterlambatan',
      'denda',
      'year_month',
      'tahun',
      'bulan',
    ];

  function __construct(){
	$this->table = 'pengembalian';
  }

  function peminjaman(){
  	return $this->belongsTo('App\Peminjaman', 'peminjaman_id');
  }

  function getKeterlambatanAttribute(){
    return (new \DateTime())
    		->diff(new \DateTime($this->peminjaman->jadwalKembali))
    		->format('%a');
  }

  function getDendaAttribute(){
  	return $this->keterlambatan * $this->peminjaman->dendaHarian;
  }

  function getYearMonthAttribute(){
    return (new \DateTime($this->tanggal))->format('Y-m');
  }

  function getTahunAttribute(){
    return (new \DateTime($this->tanggal))->format('Y');
  }

  function getBulanAttribute(){
    return (new \DateTime($this->tanggal))->format('m');
  }  

  function scopePerYearMonth($query){
    return $query->addSelect(\DB::raw('date_format(pengembalian.tanggal, "%Y-%m") `year_month`,
        year(pengembalian.tanggal) tahun,
        month(pengembalian.tanggal) bulan,
        count(pengembalian.id) jumlah_pengembalian,
        count(distinct mahasiswa_id) jumlah_mahasiswa,
        count(distinct buku_id) jumlah_buku,
        count(distinct buku.jenis_buku_id) jumlah_jenis_buku
        '))
        ->join('peminjaman', 'peminjaman.id', '=', 'pengembalian.peminjaman_id')
        ->join('buku', 'buku.id', '=', 'peminjaman.buku_id')
        ->groupBy(\DB::raw('date_format(pengembalian.tanggal, "%Y-%m")'));
  }

  function scopePerYear($query){
    return $query->addSelect(\DB::raw('year(pengembalian.tanggal) tahun,
        count(pengembalian.id) jumlah_pengembalian,
        count(distinct peminjaman.mahasiswa_id) jumlah_mahasiswa,
        count(distinct peminjaman.buku_id) jumlah_buku,
        count(distinct buku.jenis_buku_id) jumlah_jenis_buku
        '))
        ->join('peminjaman', 'peminjaman.id', '=', 'pengembalian.peminjaman_id')
        ->join('buku', 'buku.id', '=', 'peminjaman.buku_id')
        ->groupBy(\DB::raw('year(pengembalian.tanggal)'));
  }  

}
