<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
	
  function __construct(){
	$this->table = 'buku';
  }

  function jenisBuku(){
  	return $this->belongsTo('\App\JenisBuku', 'jenis_buku_id');
  }
}
