<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
  const HARI_MAX_PEMINJAMAN = 3;
  const DENDA_DASAR_PER_HARI = 1000;

  protected $appends = ['denda_harian',
      'jadwal_kembali',
      'year_month',
      'tahun',
      'bulan',
      // 'jumlah_peminjaman',
      // 'jumlah_mahasiswa',
      // 'jumlah_buku',
    ];

  function __construct(){
  	$this->table = 'peminjaman';
  }

  function mahasiswa(){
  	return $this->belongsTo('App\Mahasiswa', 'mahasiswa_id');
  }

  function buku(){
    return $this->belongsTo('\App\Buku', 'buku_id')->with('jenisBuku');
  }

  function getDendaHarianAttribute(){
    return \App\Peminjaman::DENDA_DASAR_PER_HARI * ($this->mahasiswa->semester >= 3 ? 3 : 1) + ($this->buku->jenis_buku_id = 1 ? 3000 : 0);
  }

  function getJadwalKembaliAttribute(){
    return (new \DateTime($this->tanggal))
        ->add(new \DateInterval('P'. \App\Peminjaman::HARI_MAX_PEMINJAMAN . 'D'))
        ->format('Y-m-d');
  }

  function getYearMonthAttribute(){
    return (new \DateTime($this->tanggal))->format('Y-m');
  }

  function getTahunAttribute(){
    return (new \DateTime($this->tanggal))->format('Y');
  }

  function getBulanAttribute(){
    return (new \DateTime($this->tanggal))->format('m');
  }

  // function getJumlahPeminjamanAttribute(){
  //   $model = \App\Peminjaman::perYearMonth()
  //     ->where(\DB::raw('date_format(tanggal, "%Y-%m")'), '=', $this->yearMonth)
  //     ->first();
  //   return $model->attributes['jumlah_peminjaman'];
  // }

  // function getJumlahMahasiswaAttribute(){
  //   $model = \App\Peminjaman::perYearMonth()
  //     ->where(\DB::raw('date_format(tanggal, "%Y-%m")'), '=', $this->yearMonth)
  //     ->first();
  //   return $model->attributes['jumlah_mahasiswa'];
  // }

  // function getJumlahBukuAttribute(){
  //   $model = \App\Peminjaman::perYearMonth()
  //     ->where(\DB::raw('date_format(tanggal, "%Y-%m")'), '=', $this->yearMonth)
  //     ->first();
  //   return $model->attributes['jumlah_buku'];
  // }

  function scopePerYearMonth($query){
    return $query->addSelect(\DB::raw('date_format(peminjaman.tanggal, "%Y-%m") `year_month`,
        year(peminjaman.tanggal) tahun,
        month(peminjaman.tanggal) bulan,
        count(peminjaman.id) jumlah_peminjaman,
        count(distinct peminjaman.mahasiswa_id) jumlah_mahasiswa,
        count(distinct peminjaman.buku_id) jumlah_buku,
        count(distinct buku.jenis_buku_id) jumlah_jenis_buku
        '))
        ->join('buku', 'buku.id', '=', 'peminjaman.buku_id')
        ->groupBy(\DB::raw('date_format(peminjaman.tanggal, "%Y-%m")'));
  }

  function scopePerYear($query){
    return $query->addSelect(\DB::raw('year(peminjaman.tanggal) tahun,
        count(peminjaman.id) jumlah_peminjaman,
        count(distinct peminjaman.mahasiswa_id) jumlah_mahasiswa,
        count(distinct peminjaman.buku_id) jumlah_buku,
        count(distinct buku.jenis_buku_id) jumlah_jenis_buku
        '))
        ->join('buku', 'buku.id', '=', 'peminjaman.buku_id')
        ->groupBy(\DB::raw('year(peminjaman.tanggal)'));
  }  

}
