<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisBuku extends Model
{
  function __construct(){
    $this->table = 'jenis_buku';
  }

  function bukus(){
  	return $this->hasMany('\App\Buku', 'jenis_buku_id');
  }
}
