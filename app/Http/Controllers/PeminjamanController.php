<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use View;
use App;
use HTML;
use Yajra\Datatables\Datatables;

class PeminjamanController extends Controller
{

  function __call($method, $parameters)
  {
      return Response::error('404');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function getIndex()
  {
    $peminjamans = app\Peminjaman::all();
    $title = 'Peminjaman';
    return View::make('peminjaman/index')->with(['peminjamans' => $peminjamans, 'title' => $title]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function getMonthly()
  {
    $peminjamans = app\Peminjaman::all();
    $title = 'Peminjaman - Per Bulan';
    return View::make('peminjaman/monthly')->with(['peminjamans' => $peminjamans, 'title' => $title]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function getYearly()
  {
    $peminjamans = app\Peminjaman::all();
    $title = 'Peminjaman - Per Tahun';
    return View::make('peminjaman/yearly')->with(['peminjamans' => $peminjamans, 'title' => $title]);
  }

}
