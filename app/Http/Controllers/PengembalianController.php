<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use View;
use App;
use HTML;
use Yajra\Datatables\Datatables;

class PengembalianController extends Controller
{

  function __call($method, $parameters)
  {
      return Response::error('404');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function getIndex()
  {
    $pengembalians = app\Pengembalian::all();
    $title = 'Pengembalian';
    return View::make('pengembalian/index')->with(['pengembalians' => $pengembalians, 'title' => $title]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function getMonthly()
  {
    $pengembalians = app\Pengembalian::all();
    $title = 'Pengembalian - Per Bulan';
    return View::make('pengembalian/monthly')->with(['pengembalians' => $pengembalians, 'title' => $title]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function getYearly()
  {
    $pengembalians = app\Pengembalian::all();
    $title = 'Pengembalian - Per Tahun';
    return View::make('pengembalian/yearly')->with(['pengembalians' => $pengembalians, 'title' => $title]);
  }

}
