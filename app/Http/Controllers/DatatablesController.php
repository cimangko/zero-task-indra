<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use View;
use App;
use HTML;
use Yajra\Datatables\Datatables;

class DatatablesController extends Controller
{

  function dataPeminjaman(){
    $query = app\Peminjaman::with(['mahasiswa', 'buku', 'buku.jenisBuku'])->get();

    return App\Cimangko\CimangkoDatatables::of($query)
        ->make(true);

  }

  function dataPeminjamanMonthly(){
    $query = App\Peminjaman::select()->perYearMonth()->get();

    return App\Cimangko\CimangkoDatatables::of($query)
        ->make(true);
  }

  function dataPeminjamanYearly(){
    $query = App\Peminjaman::select()->perYear()->get();

    return App\Cimangko\CimangkoDatatables::of($query)
        ->make(true);
  }

  function dataPengembalian(){
    $query = app\Pengembalian::get();

    return App\Cimangko\CimangkoDatatables::of($query)
        ->make(true);

  }
  
  function dataPengembalianMonthly(){
    $query = App\Pengembalian::with(['peminjaman', 'peminjaman.mahasiswa', 'peminjaman.buku', 'peminjaman.buku.jenisBuku'])->select()->perYearMonth()->get();

    return App\Cimangko\CimangkoDatatables::of($query)
        ->make(true);
  }

  function dataPengembalianYearly(){
    $query = App\Pengembalian::with(['peminjaman', 'peminjaman.mahasiswa', 'peminjaman.buku', 'peminjaman.buku.jenisBuku'])->select()->perYear()->get();

    return App\Cimangko\CimangkoDatatables::of($query)
        ->make(true);
  }

  /**
   * Process datatables ajax request.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function anyData(Request $request, $one)
  {
    switch($one){
      case 'pinjam':
        return $this->dataPeminjaman();
        break;
      case 'kembali':
        return $this->dataPengembalian();
        break;
      case 'pinjam-monthly':
        return $this->dataPeminjamanMonthly();
        break;
      case 'pinjam-yearly':
        return $this->dataPeminjamanYearly();
        break;
      case 'kembali-monthly':
        return $this->dataPengembalianMonthly();
        break;
      case 'kembali-yearly':
        return $this->dataPengembalianYearly();
        break;
      default:
        return json_encode(null);
        break;
    }
  }




}
