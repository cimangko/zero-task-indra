@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')

@section('content')
    <table class="table table-bordered" id="peminjaman-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Tanggal Kembali</th>
                <th>Mahasiswa</th>
                <th>Buku</th>
                <th>Denda Per Hari</th>
                <th>Jadwal Kembali</th>
                <th>Keterlambatan</th>
                <th>Denda</th>
            </tr>
        </thead>
    </table>

@stop

@push('scripts')
<script>
$(function() {
    $('#peminjaman-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.data') !!}/kembali',
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        columns: [
            { data: 'id', name: 'id' },
            { data: 'tanggal', name: 'tanggal' },
            { data: 'peminjaman.mahasiswa.nama', name: 'peminjaman.mahasiswa.nama' },
            { data: 'peminjaman.buku.judul', name: 'peminjaman.buku.judul' },
            { data: 'peminjaman.denda_harian', name: 'denda_harian' },
            { data: 'peminjaman.jadwal_kembali', name: 'jadwal_kembali' },
            { data: 'keterlambatan', name: 'keterlambatan' },
            { data: 'denda', name: 'denda' },
        ]
    });

});
</script>
@endpush            
@endsection
