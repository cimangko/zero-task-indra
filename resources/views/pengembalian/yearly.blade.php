@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')

@section('content')

    <table class="table table-bordered" id="yearly-pengembalian-table">
        <thead>
            <tr>
                <th>Tahun</th>
                <th>Jumlah Pengembalian</th>
                <th>Jumlah Mahasiswa</th>
                <th>Jumlah Buku</th>
                <th>Jumlah Jenis Buku</th>
            </tr>
        </thead>
    </table>
@stop

@push('scripts')
<script>
$(function() {

    $('#yearly-pengembalian-table').DataTable({
        processing: true,
        serverSide: true,
        // ajax: '{!! route('datatables.data') !!}',
        ajax: '{!! route('datatables.data') !!}/kembali-yearly',
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        columns: [
            { data: 'tahun', name: 'tahun' },
            { data: 'jumlah_pengembalian', name: 'jumlah_pengembalian' },
            { data: 'jumlah_mahasiswa', name: 'jumlah_mahasiswa' },
            { data: 'jumlah_buku', name: 'jumlah_buku' },
            { data: 'jumlah_jenis_buku', name: 'jumlah_jenis_buku' },
        ]
    });
});
</script>
@endpush            
@endsection
