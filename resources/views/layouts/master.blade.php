<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Zero Task Indra Ginanjar - Perpustakaan</title>

        <!-- Bootstrap CSS -->
        <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"> -->
        <link rel="stylesheet" href="{{ URL::asset('vendor/bootstrap/bootstrap.min.css') }}">
        <!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> -->
        <link rel="stylesheet" href="{{ URL::asset('vendor/datatables/1.10.15/css/jquery.dataTables.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('vendor/datatables/buttons/1.4.2/css/buttons.dataTables.min.css') }}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            body {
                padding-top: 40px;
            }
        </style>
    </head>
    <body>

         <nav class="navbar navbar-inverse">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="#">Zero Task</a>
            </div>
            <ul class="nav navbar-nav">
              <!-- <li class="active"><a href="{{url('/')}}">Home</a></li> -->
              <li><a href="{{url('/peminjaman')}}">Peminjaman</a></li>
              <li><a href="{{url('/pengembalian')}}">Pengembalian</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="#"><span class="glyphicon glyphicon-user"></span>Indra Ginanjar</a></li>
            </ul>
          </div>
        </nav> 

        <div class="row" style="margin-bottom: 2em">
            <div class="col-lg-1"></div>
            <div class="col-lg-10" style="background-color: #232323;color:white;padding:1em">
Peminjaman dan Pengembalian buku perpustakaan dengan denda conditional dimana mahasiswa semester > 3 denda 2x dari mahasiswa semester < 3, dan denda per jenis buku berbeda misal buku jenis fiksi ilmiah denda 3000 lebih banyak dari denda buku novel / komik.

input data dengan faker minimal 100 data peminjaman dan 50 pengembalian.

buat report dari case di atas dengan sorting ajax mahasiswa dan jenis buku dalam view datatable dan excel / pdf

auth menggunakan default laravel.
            </div>
        </div>

        <div class="container">
            @yield('content')
        </div>

        <!-- jQuery -->
        <!-- <script src="//code.jquery.com/jquery.js"></script> -->
        <script src="{{ URL::asset('vendor/jquery/jquery-2.2.4.min.js') }}"></script>
        <!-- DataTables -->
        <!-- <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
        <script src="{{ URL::asset('vendor/datatables/1.10.15/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ URL::asset('vendor/datatables/buttons/1.4.2/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ URL::asset('vendor/datatables/buttons/1.4.2/js/buttons.html5.min.js') }}"></script>
        <script src="{{ URL::asset('vendor/datatables/jszip/3.1.3/jszip.min.js') }}"></script>
        <script src="{{ URL::asset('vendor/datatables/pdfmake/0.1.32/pdfmake.min.js') }}"></script>
        <script src="{{ URL::asset('vendor/datatables/pdfmake/0.1.32/vfs_fonts.js') }}"></script>
        <!-- Bootstrap JavaScript -->
        <!-- <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
        <script src="{{ URL::asset('vendor/bootstrap/bootstrap.min.js') }}"></script>
        <!-- App scripts -->
        @stack('scripts')
    </body>
</html>