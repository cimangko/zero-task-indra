<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{$title}}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="{{ URL::asset('vendor/bootstrap/bootstrap.min.css') }}">
    <!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> -->
    <link rel="stylesheet" href="{{ URL::asset('vendor/datatables/1.10.15/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('vendor/datatables/buttons/1.4.2/css/buttons.dataTables.min.css') }}">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Zero Task
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/home') }}">Home</a></li>
                    @if (Auth::user())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Peminjaman <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{url('/peminjaman')}}">Daftar</a></li>
                            <li><a href="{{url('/peminjaman/monthly')}}">Laporan Bulanan</a></li>
                            <li><a href="{{url('/peminjaman/yearly')}}">Laporan Tahunan</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Pengembalian <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{url('/pengembalian')}}">Daftar</a></li>
                            <li><a href="{{url('/pengembalian/monthly')}}">Laporan Bulanan</a></li>
                            <li><a href="{{url('/pengembalian/yearly')}}">Laporan Tahunan</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

        <div class="row" style="margin-bottom: 2em">
            <div class="col-lg-1"></div>
            <div class="col-lg-10" style="background-color: #FFe7e7;padding:1em">
Peminjaman dan Pengembalian buku perpustakaan dengan denda conditional dimana mahasiswa semester > 3 denda 2x dari mahasiswa semester < 3, dan denda per jenis buku berbeda misal buku jenis fiksi ilmiah denda 3000 lebih banyak dari denda buku novel / komik.

input data dengan faker minimal 100 data peminjaman dan 50 pengembalian.

buat report dari case di atas dengan sorting ajax mahasiswa dan jenis buku dalam view datatable dan excel / pdf

auth menggunakan default laravel.
            </div>
        </div>


    @yield('content')

    <!-- JavaScripts -->
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  -->   <script src="{{ URL::asset('vendor/jquery/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ URL::asset('vendor/datatables/1.10.15/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('vendor/datatables/buttons/1.4.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('vendor/datatables/buttons/1.4.2/js/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('vendor/datatables/jszip/3.1.3/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('vendor/datatables/pdfmake/0.1.32/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('vendor/datatables/pdfmake/0.1.32/vfs_fonts.js') }}"></script>
    <script src="{{ URL::asset('vendor/bootstrap/bootstrap.min.js') }}"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
        @stack('scripts')
</body>
</html>
