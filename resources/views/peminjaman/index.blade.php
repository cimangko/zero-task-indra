@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')

@section('content')
    <table class="table table-bordered" id="peminjaman-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Tanggal</th>
                <th>Mahasiswa</th>
                <th>Buku</th>
                <th>Jenis Buku</th>
                <th>Denda Per Hari</th>
                <th>Jadwal Kembali</th>
            </tr>
        </thead>
    </table>

@stop

@push('scripts')
<script>
$(function() {
    $('#peminjaman-table').DataTable({
        processing: true,
        serverSide: true,
        // ajax: '{!! route('datatables.data') !!}',
        ajax: '{!! route('datatables.data') !!}/pinjam',
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        columns: [
            { data: 'id', name: 'id' },
            { data: 'tanggal', name: 'tanggal' },
            { data: 'mahasiswa.nama', name: 'mahasiswa.nama' },
            { data: 'buku.judul', name: 'buku.judul' },
            { data: 'buku.jenis_buku.nama', name: 'buku.jenis_buku.nama' },
            { data: 'denda_harian', name: 'denda_harian' },
            { data: 'jadwal_kembali', name: 'jadwal_kembali' },
        ]
    });


});
</script>
@endpush            
@endsection
