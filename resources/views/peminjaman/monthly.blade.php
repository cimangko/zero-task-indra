@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')

@section('content')

    <table class="table table-bordered" id="monthly-peminjaman-table">
        <thead>
            <tr>
                <th>Tahun - Bulan</th>
                <th>Jumlah Peminjaman</th>
                <th>Jumlah Mahasiswa</th>
                <th>Jumlah Buku</th>
                <th>Jumlah Jenis Buku</th>
            </tr>
        </thead>
    </table>
@stop

@push('scripts')
<script>
$(function() {

    $('#monthly-peminjaman-table').DataTable({
        processing: true,
        serverSide: true,
        // ajax: '{!! route('datatables.data') !!}',
        ajax: '{!! route('datatables.data') !!}/pinjam-monthly',
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        columns: [
            { data: 'year_month', name: 'year_month' },
            { data: 'jumlah_peminjaman', name: 'jumlah_peminjaman' },
            { data: 'jumlah_mahasiswa', name: 'jumlah_mahasiswa' },
            { data: 'jumlah_buku', name: 'jumlah_buku' },
            { data: 'jumlah_jenis_buku', name: 'jumlah_jenis_buku' },
        ]
    });
});
</script>
@endpush            
@endsection
