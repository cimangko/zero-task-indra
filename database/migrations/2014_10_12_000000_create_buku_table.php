<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('buku', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('jenis_buku_id');
            $table->string('judul');

            $table->foreign('jenis_buku_id')->references('id')->on('jenis_buku'); 
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('buku');
        Schema::enableForeignKeyConstraints();
    }
}
