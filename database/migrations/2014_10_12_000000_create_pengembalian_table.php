<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengembalianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('pengembalian', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('peminjaman_id');
            $table->date('tanggal');

            $table->foreign('peminjaman_id')->references('id')->on('peminjaman'); 
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('pengembalian');
        Schema::enableForeignKeyConstraints();
    }
}
