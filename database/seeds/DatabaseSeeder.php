<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(JenisBukuSeeder::class);
    	$this->call(BukuSeeder::class);
        $this->call(MahasiswaSeeder::class);
        $this->call(PeminjamanSeeder::class);
        $this->call(PengembalianSeeder::class);

       	// factory(App\Buku::class, 200)->create()->each(function($u) {
       	// 	$u->posts()->save(factory(App\Post::class)->make());
       	// });

    }
}
