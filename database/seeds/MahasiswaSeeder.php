<?php

use Illuminate\Database\Seeder;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        for($i = 0; $i < 200; $i++){
            DB::table('mahasiswa')->insert([
                'nama' => $faker->name,
                'semester' => $faker->numberBetween(1,14),
            ]);
        }
    }
}
