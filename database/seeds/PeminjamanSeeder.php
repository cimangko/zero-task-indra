<?php

use Illuminate\Database\Seeder;

class PeminjamanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        for($i = 0; $i < 100; $i++){
            DB::table('peminjaman')->insert([
                'mahasiswa_id' => $faker->numberBetween(1,200),
                'buku_id' => $faker->numberBetween(1,200),
                'tanggal' => $faker->date,
            ]);
        }
    }
}
