<?php

use Illuminate\Database\Seeder;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 200; $i++) {
            DB::table('buku')->insert([
                'jenis_buku_id' => $faker->numberBetween(1,20),
                'judul' => $faker->sentence,
            ]);
        }
    }
}
