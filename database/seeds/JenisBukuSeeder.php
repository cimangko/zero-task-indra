<?php

use Illuminate\Database\Seeder;

class JenisBukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_buku')->insert([
            'id' => '1',
            'nama' => 'Fiksi/Ilmiah',
        ]);

        DB::table('jenis_buku')->insert([
            'id' => '2',
            'nama' => 'Novel/Komik',
        ]);

        $faker = Faker\Factory::create();

        for ($i = 0; $i < 18; $i++) {
            DB::table('jenis_buku')->insert([
                'nama' => $faker->word,
            ]);
        }
    }
}
