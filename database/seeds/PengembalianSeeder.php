<?php

use Illuminate\Database\Seeder;

class PengembalianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        for($i = 0; $i < 50; $i++){
            DB::table('pengembalian')->insert([
                'peminjaman_id' => $faker->numberBetween(1,100),
                'tanggal' => $faker->date,
            ]);
        }
    }
}
